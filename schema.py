from marshmallow import validate
from core import ma
import marshmallow
import models
from marshmallow_enum import EnumField


####################
# REQUEST SCHEMAS #
####################
class StartEndDateRequestSchema(ma.Schema):
    start_date = marshmallow.fields.Str()
    end_date = marshmallow.fields.Str()


class UserLoginRequestSchema(ma.Schema):
    username = marshmallow.fields.Str(required=True)
    password = marshmallow.fields.Str(required=True)


class UserPasswordConfirmSchema(UserLoginRequestSchema):
    password_confirm = marshmallow.fields.Str(required=True)
    first_name = marshmallow.fields.Str(required=True)
    last_name = marshmallow.fields.Str(required=True)


class UserRequiredRequestSchema(UserLoginRequestSchema):
    first_name = marshmallow.fields.Str(required=True)
    last_name = marshmallow.fields.Str(required=True)
    role = marshmallow.fields.Str(required=True)
    password_confirm = marshmallow.fields.Str(required=True)


class UserEditRequestSchema(ma.Schema):
    username = marshmallow.fields.Str()
    password = marshmallow.fields.Str()
    first_name = marshmallow.fields.Str()
    last_name = marshmallow.fields.Str()
    role = marshmallow.fields.Str()
    password_confirm = marshmallow.fields.Str()
    id = marshmallow.fields.Int()


class CustomerRequestSchema(ma.Schema):
    name = marshmallow.fields.Str(required=True)
    type = marshmallow.fields.Str(required=True)
    email = marshmallow.fields.Str(required=True)
    phone = marshmallow.fields.Str()
    pib_jmbg = marshmallow.fields.Str()
    address = marshmallow.fields.Str()


class CustomerEditRequestSchema(ma.Schema):
    name = marshmallow.fields.Str()
    type = marshmallow.fields.Str()
    email = marshmallow.fields.Str()
    phone = marshmallow.fields.Str()
    pib_jmbg = marshmallow.fields.Str()
    address = marshmallow.fields.Str()
    id = marshmallow.fields.Int()


class ApartmentRequestSchema(ma.Schema):
    lamella = marshmallow.fields.Str(required=True)
    square_footage = marshmallow.fields.Int(required=True)
    floor = marshmallow.fields.Int(required=True)
    rooms = marshmallow.fields.Int(required=True)
    price = marshmallow.fields.Int(required=True)
    orientation = marshmallow.fields.Str()
    balconies = marshmallow.fields.Int()
    status = marshmallow.fields.Str()
    photo = marshmallow.fields.Str(allow_none=True)


class ApartmentEditSchema(ma.Schema):
    floor = marshmallow.fields.Int()
    rooms = marshmallow.fields.Int()
    orientation = marshmallow.fields.Str()
    balconies = marshmallow.fields.Int()
    lamella = marshmallow.fields.Str()
    photo = marshmallow.fields.Str()
    price = marshmallow.fields.Int()
    square_footage = marshmallow.fields.Int()


class ApartmentFilterRequestSchema(ApartmentEditSchema):
    square_footage_gt = marshmallow.fields.Int()
    square_footage_lt = marshmallow.fields.Int()
    price_gt = marshmallow.fields.Int()
    price_lt = marshmallow.fields.Int()
    status = marshmallow.fields.Str()


class ContractEditSchema(StartEndDateRequestSchema):
    user_id = marshmallow.fields.Int()
    payment_method = marshmallow.fields.Str()
    first_visit = marshmallow.fields.Date()
    status = marshmallow.fields.Str()
    approved_by = marshmallow.fields.Int()
    approved = marshmallow.fields.Bool()
    signed = marshmallow.fields.Bool()
    note = marshmallow.fields.Str()
    contract_number = marshmallow.fields.Str()
    price = marshmallow.fields.Int()


class ContractRequestSchema(ContractEditSchema):
    customer_id = marshmallow.fields.Int()
    apartment_id = marshmallow.fields.Int()


####################
# RESPONSE SCHEMAS #
####################
class UserSchema(ma.SQLAlchemyAutoSchema):
    class Meta:
        model = models.User
        exclude = ['password']

    role = EnumField(models.user.UserRoleEnum)


class CustomerSchema(ma.SQLAlchemyAutoSchema):
    class Meta:
        model = models.Customer

    type = EnumField(models.user.CustomerTypeEnum)


class ApartmentSchema(ma.SQLAlchemyAutoSchema):
    class Meta:
        model = models.Apartment

    status = EnumField(models.user.ApartmentStatusEnum)
    orientation = EnumField(models.user.ApartmentOrientationEnum)


class ContractSchema(ma.SQLAlchemyAutoSchema):
    class Meta:
        model = models.Contract

    user = ma.Nested(UserSchema)
    customer = ma.Nested(CustomerSchema)
    payment_method = EnumField(models.user.PaymentMethodEnum)
    status = EnumField(models.user.CustomerApartmentStatusEnum)
    apartment = ma.Nested(ApartmentSchema, exclude=['photo'])
